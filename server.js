//## https://beige-haruasm.c9users.io/
//https://Daynys@bitbucket.org/Daynys/codej.git

// ## 노드 내장 모듈
var http = require('http')
  , path = require('path')
  , fs = require('fs')
  ;

// ## express middlewares
const express = require('express')
  , favicon = require('serve-favicon')
//  , logger = require('morgan')
//  , methodOverride = require('method-override')
//  , session = require('express-session')
  , bodyParser = require('body-parser')
//  , multer = require('multer')
  , errorHandler = require('errorhandler')
  , IO = require('socket.io')
  ;

  //## 생성
var app = express();
//var mupload = multer({dest: './uploads'});


//## all environments
app.set('env', 'development')
   .set('port', process.env.NODEPORT || process.argv[2] || 8199)
   //.set('views', __dirname + '/views')
   //.set('view engine', 'pug') // use jade -> pug
   //.set('view engine', 'html')
   .use(favicon(__dirname+'/public/images/favicon.ico'))
//   .use(logger('dev'))
//   .use(methodOverride())
//   .use(session({ resave: true,
//                 saveUninitialized: true,
//                 secret: 'wiricy8' }))
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: true}))
   ;

//## middleware
app.use(express.static(path.join(__dirname, 'public')))
   ;

//## router
app.route('/wed')
    .get((req,res,next)=>{
        console.log('wed');
        res.end('idx');
    })

    ;

app.route('/')
    .get((req,res,next)=>{

        console.log('hi');

        res.sendFile( path.join(__dirname, 'public' , 'index.html'));

    });




if('development' == app.get('env'))
    app
        .use(errorHandler())
        ;

else if('production' == app.get('env')){
    var compression = require('compression');
    app.use(compression());
}

// http listening
var httpServer  = http.createServer(app).listen(app.get('port'), ()=>{
    console.log(`Express::http listening on *:${app.get('port')}`);
});
